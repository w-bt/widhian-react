import React, { Component } from "react";
import DataService from "../services/service";
import { Editor } from "@tinymce/tinymce-react";
import "bootstrap/dist/css/bootstrap.min.css";
import swal from 'sweetalert';

class EditArticle extends Component {
    constructor(props) {
        super(props);
        this.getCategories = this.getCategories.bind(this);
        this.getArticle = this.getArticle.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeCategory = this.onChangeCategory.bind(this);
        this.onChangePublished = this.onChangePublished.bind(this);
        this.updateArticle = this.updateArticle.bind(this);

        this.state = {
            categories: [],
            currentArticle: {
                id: 0,
                title: "",
                description: "",
                categoryId: 0,
                published: false,
                createdAt: null,
                updatedAt: null,
                comments: [],
                category: {
                    name: '',
                    slug: '',
                },
            },
        };
    }

    componentDidMount() {
        const { year, month, date, slug } = this.props.match.params;

        this.getCategories();
        this.getArticle(year, month, date, slug);
    }

    getCategories() {
        DataService.getAllCategories()
            .then(response => {
                this.setState({
                    categories: response.data,
                    category: response.data[0].id
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    getArticle(year, month, date, slug) {
        DataService.getArticleBySlug(year, month, date, slug)
            .then(response => {
                this.setState({
                    currentArticle: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    onChangeTitle(e) {
        this.setState({
            currentArticle: {
                ...this.state.currentArticle,
                title: e.target.value
            }
        });
    }

    onChangeDescription(e) {
        this.setState({
            currentArticle: {
                ...this.state.currentArticle,
                description: e.target.getContent()
            }
        });
    }

    onChangeCategory(e) {
        this.setState({
            currentArticle: {
                ...this.state.currentArticle,
                categoryId: e.target.value
            }
        });
    }

    onChangePublished(e) {
        this.setState({
            currentArticle: {
                ...this.state.currentArticle,
                published: e.target.value === "true"
            }
        });
    }

    updateArticle() {
        const data = {
            title: this.state.currentArticle.title,
            description: this.state.currentArticle.description,
            categoryId: this.state.currentArticle.categoryId,
            published: this.state.currentArticle.published
        };

        DataService.updateArticle(this.state.currentArticle.id, data)
            .then(response => {
                swal({
                    title: "Done!",
                    text: "article is updated in database",
                    icon: "success",
                    timer: 2000,
                    button: false
                }).then(function() {
                    window.location = "/#/admin/article/list";
                });
                console.log(response.data);
            })
            .catch(e => {
                swal({
                    title: "Failed!",
                    text: "article is updated to delete",
                    icon: "error",
                    timer: 2000,
                    button: false
                })
                console.log(e);
            });
    }

    render() {
        const { categories } = this.state;

        const categoryOption = function (category) {
            return (
                <option key={category.id} value={category.id}>{category.name}</option>
            );
        }

        const categoriesOption = categories.map(categoryOption.bind(this));

        return (
            <div className="submit-form">
                <div>
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input
                            type="text"
                            className="form-control"
                            id="title"
                            required
                            value={this.state.currentArticle.title}
                            onChange={this.onChangeTitle}
                            name="title"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <Editor
                            initialValue={this.state.currentArticle.description}
                            apiKey='j8ugmvtiqjmrudy8bd1qttisquw5ngtnqr721yg208fbgjji'
                            init={{
                                height: 500,
                                menubar: true,
                                plugins: [
                                    'advlist autolink lists link image',
                                    'charmap print preview anchor',
                                    'searchreplace visualblocks code',
                                    'insertdatetime media table paste wordcount image imagetools pagebreak codesample'
                                ],
                                toolbar:
                                    'undo redo | formatselect | bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image media codesample | anchor pagebreak code',
                                paste_data_images: true,
                                image_advtab: true,
                                imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
                                codesample_languages: [
                                    { text: 'Golang', value: 'go' },
                                    { text: 'HTML/XML', value: 'markup' },
                                    { text: 'JavaScript', value: 'javascript' },
                                    { text: 'CSS', value: 'css' },
                                    { text: 'PHP', value: 'php' },
                                    { text: 'Ruby', value: 'ruby' },
                                    { text: 'Python', value: 'python' },
                                    { text: 'Java', value: 'java' },
                                    { text: 'C', value: 'c' },
                                    { text: 'C#', value: 'csharp' },
                                    { text: 'C++', value: 'cpp' }
                                ],
                            }}
                            onChange={this.onChangeDescription}
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="title">Category</label>
                        <select value={this.state.currentArticle.categoryId} className="form-control" id="category" name="category" onChange={this.onChangeCategory}>
                            {categoriesOption}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="status">Status</label>
                        <br />
                        <input type="radio" name="published"
                               value="true"
                               checked={this.state.currentArticle.published.toString() === "true"}
                               onChange={this.onChangePublished} /> published&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="draft"
                               value="false"
                               checked={this.state.currentArticle.published.toString() === "false"}
                               onChange={this.onChangePublished} /> draft
                    </div>

                    <button onClick={this.updateArticle} className="btn btn-success">
                        Submit
                    </button>
                </div>
            </div>
        );
    }
}

export default EditArticle;