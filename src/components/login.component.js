import React, { useState } from 'react';
import { setUserSession } from '../utils/common';
import DataService from "../services/service";

function Login() {
    const email = useFormInput('');
    const password = useFormInput('');
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    // handle button click of login form
    const handleLogin = () => {
        setError(null);
        setLoading(true);
        DataService.login({ email: email.value, password: password.value })
            .then(response => {
                setLoading(false);
                setUserSession(response.data.token, response.data.user);
                window.location.href="/";
            })
            .catch(error => {
                setLoading(false);
                if (error.response.status === 401) setError(error.response.data.message);
                else setError("Something went wrong. Please try again later.");
            });
    }

    return (
        <div>
            <h3>Login</h3><br /><br />
            <div>
                Email<br />
                <input type="text" className="form-control" {...email} autoComplete="new-password" />
            </div>
            <div style={{ marginTop: 10 }}>
                Password<br />
                <input type="password" {...password} className="form-control" autoComplete="new-password" />
            </div>
            {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
            <input type="button" className="btn btn-info" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} /><br />
        </div>
    );
}

const useFormInput = initialValue => {
    const [value, setValue] = useState(initialValue);

    const handleChange = e => {
        setValue(e.target.value);
    }
    return {
        value,
        onChange: handleChange
    }
}

export default Login;