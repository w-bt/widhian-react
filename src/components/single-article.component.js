import React, { Component } from "react";
import DataService from "../services/service";
import { Link } from "react-router-dom";
import "../prism.css";
import "../prism.js";

class SingleArticle extends Component {
    constructor(props) {
        super(props);
        this.getArticle = this.getArticle.bind(this);

        this.state = {
            currentArticle: {
                id: 0,
                title: "",
                description: "",
                categoryId: 0,
                published: false,
                createdAt: null,
                updatedAt: null,
                comments: [],
                category: {
                    name: '',
                    slug: '',
                },
            },
            message: ""
        };
    }

    componentDidMount() {
        const { year, month, date, slug } = this.props.match.params;
        this.getArticle(year, month, date, slug);
    }

    getArticle(year, month, date, slug) {
        DataService.getArticleBySlug(year, month, date, slug)
            .then(response => {
                this.setState({
                    currentArticle: response.data
                });
                console.log(response.data);
                window.Prism.highlightAll();
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { currentArticle } = this.state;

        let articleDate = null;
        let year = '';
        let month = '';
        let day = '';
        let fullDate = '';
        let categoryName = '';
        let path = '';
        let readMore = '<!-- pagebreak -->';

        if(currentArticle) {
            articleDate = new Date(currentArticle.createdAt);
            year = articleDate.getFullYear();
            month = articleDate.getMonth() + 1;
            day = articleDate.getDate();
            fullDate = articleDate.toDateString() + ' at ' + articleDate.toLocaleTimeString();
            categoryName = currentArticle.category.name;
            path = '/' + year + '/' + month + '/' + day + '/' + currentArticle.slug;
        }

        return (
            <div className="container" style={{marginTop: "50px"}}>
                {currentArticle ? (
                    <div className="row">
                        <div className="col col-lg-12">
                            <div className="post-title">
                                <h1><Link to={path} style={{textDecoration: "none", color: "inherit"}}><b>{currentArticle.title}</b></Link></h1>
                            </div>

                            <div className="post-info">
                                <p>{fullDate} / In <b>{categoryName}</b></p>
                            </div>

                            <hr />

                            <article style={{marginTop: "30px"}}>
                                { <div dangerouslySetInnerHTML={{ __html: currentArticle.description.replace(readMore, '') }} /> }
                            </article>
                        </div>
                    </div>
                ) : (
                    <div className="row">
                        <div className="col col-lg-12">
                            <p>Article not found</p>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default SingleArticle;