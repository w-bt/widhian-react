import React, { Component } from "react";
import DataService from "../services/service";
import {Link} from "react-router-dom";
import "../prism.css";
import "../prism.js";

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.retrieveArticles = this.retrieveArticles.bind(this);

        this.state = {
            articles: [],
            page: 1,
        };
    }

    componentDidMount() {
        this.retrieveArticles();
    }

    retrieveArticles() {
        const query = new URLSearchParams(this.props.location.search);
        let limit = 5;
        let offset = 0;
        let page = query.get('page');
        if(page){
            page = parseInt(page);
            if(page > 0) {
                offset = (page - 1) * limit;
                this.setState({
                    page: page
                });
            }
        } else {
            this.setState({
                page: 1
            });
        }

        DataService.getAllPublishedArticles(limit, offset)
            .then(response => {
                this.setState({
                    articles: response.data
                });
                console.log(response.data);
                window.Prism.highlightAll();
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { articles, page } = this.state;

        const preProcess = (article) => {
            let articleDate = new Date(article.createdAt);
            let year = articleDate.getFullYear();
            let month = articleDate.getMonth() + 1;
            let day = articleDate.getDate();
            let fullDate = articleDate.toDateString() + ' at ' + articleDate.toLocaleTimeString();
            let categoryName = article.category.name;
            let path = '/' + year + '/' + month + '/' + day + '/' + article.slug;
            let readMore = '<!-- pagebreak -->';
            let rawArticle = article.description.split(readMore);
            let readMoreSection = '';

            if(rawArticle.length > 1) {
                readMoreSection = (
                    <div className="read-more">
                        <p><Link to={path}>Read More ...</Link></p>
                    </div>
                )
            }

            return (
                <div className="row" key={article.slug}>
                    <div className="col col-lg-12">
                        <div className="post-title">
                            <h1><Link to={path} style={{textDecoration: "none", color: "inherit"}}><b>{article.title}</b></Link></h1>
                        </div>

                        <div className="post-info">
                            <p>{fullDate} / In <b>{categoryName}</b></p>
                        </div>

                        <article style={{marginTop: "30px"}}>
                            { <div dangerouslySetInnerHTML={{ __html: rawArticle[0] }} /> }
                        </article>

                        {readMoreSection}

                        <hr />
                    </div>
                </div>
            );
        }

        const finalArticles = articles.map(preProcess.bind(this));

        const getNavigation = () => {
            let prevPage = "/?page=" + (page - 1);
            let nextPage = "/?page=" + (page + 1);

            return (
                <div className="float-right">
                    {(page > 1) ? (
                        <div className="float-right">
                            <a href={prevPage}>&lt; Prev</a>&nbsp;&nbsp;&nbsp;<a href={nextPage}>Next &gt;</a>
                        </div>
                    ) : (
                        <div className="float-right">
                            <a href={nextPage}>Next &gt;</a>
                        </div>
                    )}
                </div>
            )
        }

        return (
            <div className="container" style={{marginTop: "50px"}}>
                {finalArticles}
                <div className="col col-md-12">
                    {getNavigation()}
                </div>
            </div>
        );
    }
}

export default HomePage;