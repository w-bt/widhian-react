import React, { Component } from "react";
import DataService from "../services/service";
import { Editor } from "@tinymce/tinymce-react";
import "bootstrap/dist/css/bootstrap.min.css";
import swal from 'sweetalert';

class AddArticle extends Component {
    constructor(props) {
        super(props);
        this.getCategories = this.getCategories.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeCategory = this.onChangeCategory.bind(this);
        this.saveArticle = this.saveArticle.bind(this);
        this.publishArticle = this.publishArticle.bind(this);
        this.createArticle = this.createArticle.bind(this);

        this.state = {
            id: null,
            title: "",
            description: "",
            categories: [],
            category: "",
            published: false
        };
    }

    componentDidMount() {
        this.getCategories();
    }

    getCategories() {
        DataService.getAllCategories()
            .then(response => {
                this.setState({
                    categories: response.data,
                    category: response.data[0].id
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.getContent()
        });
    }

    onChangeCategory(e) {
        this.setState({
            category: e.target.value
        });
    }

    publishArticle() {
        const data = {
            title: this.state.title,
            description: this.state.description,
            categoryId: this.state.category,
            published: true
        };

        this.createArticle(data);
    }

    saveArticle() {
        const data = {
            title: this.state.title,
            description: this.state.description,
            categoryId: this.state.category,
            published: false
        };

        this.createArticle(data);
    }

    createArticle(data) {
        DataService.createArticle(data)
            .then(() => {
                swal({
                    title: "Done!",
                    text: "article is submitted",
                    icon: "success",
                    timer: 2000,
                    button: false
                }).then(function() {
                    window.location = "/#/admin/article/list";
                });
            })
            .catch(e => {
                swal({
                    title: "Failed!",
                    text: "article is failed to submit",
                    icon: "error",
                    timer: 2000,
                    button: false
                })
                console.log(e);
            });
    }

    render() {
        const { categories } = this.state;

        const categoryOption = function (category) {
            return (
                <option key={category.id} value={category.id}>{category.name}</option>
            );
        }

        const categoriesOption = categories.map(categoryOption.bind(this));

        return (
            <div className="submit-form">
                <div>
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input
                            type="text"
                            className="form-control"
                            id="title"
                            required
                            value={this.state.title}
                            onChange={this.onChangeTitle}
                            name="title"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <Editor
                            initialValue={this.state.description}
                            apiKey='j8ugmvtiqjmrudy8bd1qttisquw5ngtnqr721yg208fbgjji'
                            init={{
                                height: 500,
                                menubar: true,
                                plugins: [
                                    'advlist autolink lists link image',
                                    'charmap print preview anchor',
                                    'searchreplace visualblocks code',
                                    'insertdatetime media table paste wordcount image imagetools pagebreak codesample'
                                ],
                                toolbar:
                                    'undo redo | formatselect | bold italic underline strikethrough | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image media codesample | anchor pagebreak code',
                                paste_data_images: true,
                                image_advtab: true,
                                imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
                                codesample_languages: [
                                    { text: 'Golang', value: 'go' },
                                    { text: 'HTML/XML', value: 'markup' },
                                    { text: 'JavaScript', value: 'javascript' },
                                    { text: 'CSS', value: 'css' },
                                    { text: 'PHP', value: 'php' },
                                    { text: 'Ruby', value: 'ruby' },
                                    { text: 'Python', value: 'python' },
                                    { text: 'Java', value: 'java' },
                                    { text: 'C', value: 'c' },
                                    { text: 'C#', value: 'csharp' },
                                    { text: 'C++', value: 'cpp' }
                                ],
                            }}
                            onChange={this.onChangeDescription}
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="title">Category</label>
                        <select value={this.state.category} className="form-control" id="category" name="category" onChange={this.onChangeCategory}>
                            {categoriesOption}
                        </select>
                    </div>

                    <button onClick={this.publishArticle} className="btn btn-success">
                        Submit
                    </button>

                    <button onClick={this.saveArticle} className="btn btn-info" style={{marginLeft:"5px"}}>
                        Save as Draft
                    </button>
                </div>
            </div>
        );
    }
}

export default AddArticle;