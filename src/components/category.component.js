import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import DataService from "../services/service";
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import swal from 'sweetalert';


class Category extends Component {
    constructor(props) {
        super(props);
        this.getCategories = this.getCategories.bind(this);
        this.saveCategory = this.saveCategory.bind(this);
        this.deleteCategory = this.deleteCategory.bind(this);
        this.updateCategory = this.updateCategory.bind(this);
        this.onChangeName = this.onChangeName.bind(this);

        this.state = {
            categories: [],
            isShowCreateModal: false,
            isShowDeleteModal: false,
            isShowUpdateModal: false,
            currentID: 0,
            currentName: '',
        };
    }

    componentDidMount() {
        this.getCategories();
    }

    onChangeName(e) {
        this.setState({
            currentName: e.target.value
        });
    }

    onClickCreateModal() {
        let isShowCreateModal = !this.state.isShowCreateModal;

        this.setState({
            isShowCreateModal: isShowCreateModal
        });
    }

    onClickDeleteModal(e) {
        let isShowDeleteModal = !this.state.isShowDeleteModal;

        this.setState({
            isShowDeleteModal: isShowDeleteModal,
            currentID: isShowDeleteModal ? e.target.id : 0,
            currentName: isShowDeleteModal ? e.target.name : ''
        });
    }

    onClickUpdateModal(e) {
        let isShowUpdateModal = !this.state.isShowUpdateModal;

        this.setState({
            isShowUpdateModal: isShowUpdateModal,
            currentID: isShowUpdateModal ? e.target.id : 0,
            currentName: isShowUpdateModal ? e.target.name : ''
        });
    }

    getCategories() {
        DataService.getAllCategories()
            .then(response => {
                this.setState({
                    categories: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    saveCategory() {
        const data = {
            name: this.state.currentName
        };

        DataService.createCategory(data)
            .then(response => {
                this.setState({
                    isShowCreateModal: false,
                    currentID: 0,
                    currentName: '',
                });
                this.getCategories();
                this.forceUpdate();
                swal({
                    title: "Done!",
                    text: "category is added to database",
                    icon: "success",
                    timer: 2000,
                    button: false
                })
                console.log(response.data);
            })
            .catch(e => {
                this.setState({
                    isShowCreateModal: false
                });
                swal({
                    title: "Failed!",
                    text: "category is failed to database",
                    icon: "error",
                    timer: 2000,
                    button: false
                })
                console.log(e);
            });
    }

    updateCategory() {
        const { currentName, currentID } = this.state;
        const data = {
            name: currentName
        };

        console.log(currentName);

        DataService.updateCategory(currentID, data)
            .then(response => {
                this.setState({
                    isShowUpdateModal: false,
                    currentID: 0,
                    currentName: '',
                });
                this.getCategories();
                this.forceUpdate();
                swal({
                    title: "Done!",
                    text: "category is updated in database",
                    icon: "success",
                    timer: 2000,
                    button: false
                })
                console.log(response.data);
            })
            .catch(e => {
                this.setState({
                    isShowUpdateModal: false
                });
                swal({
                    title: "Failed!",
                    text: "category is failed to update",
                    icon: "error",
                    timer: 2000,
                    button: false
                })
                console.log(e);
            });
    }

    deleteCategory() {
        DataService.deleteCategory(this.state.currentID)
            .then(response => {
                this.setState({
                    isShowDeleteModal: false,
                    currentID: 0,
                    currentName: '',
                });
                this.getCategories();
                this.forceUpdate();
                swal({
                    title: "Done!",
                    text: "category is deleted in database",
                    icon: "success",
                    timer: 2000,
                    button: false
                })
                console.log(response.data);
            })
            .catch(e => {
                this.setState({
                    isShowDeleteModal: false,
                    currentID: 0,
                    currentName: '',
                });
                swal({
                    title: "Done!",
                    text: "category is failed to delete",
                    icon: "error",
                    timer: 2000,
                    button: false
                })
                console.log(e);
            });
    }

    renderCategoryFormModal = () => {
        const { isShowCreateModal, isShowUpdateModal, currentName } = this.state;

        const onClickModal = isShowCreateModal ? this.onClickCreateModal.bind(this) : isShowUpdateModal ? this.onClickUpdateModal.bind(this) : null;
        const submitAction = isShowCreateModal ? this.saveCategory : isShowUpdateModal ? this.updateCategory : null;
        const title = isShowCreateModal ? 'Add' : isShowUpdateModal ? 'Update' : null;

        return (
            <Modal show={isShowCreateModal || isShowUpdateModal} onHide={onClickModal}>
                <Modal.Header>
                    <Modal.Title>{title} Category</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="submit-form">
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input
                                type="text"
                                className="form-control"
                                id="name"
                                required
                                value={currentName}
                                onChange={this.onChangeName}
                                name="name"
                            />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={onClickModal} type="button" className="btn btn-danger">Cancel</button>
                    <button type="button" className="btn btn-success" onClick={submitAction}>{title}</button>
                </Modal.Footer>
            </Modal>
        );
    };

    renderCategoryDeletionFormModal = () => {
        const { isShowDeleteModal, currentName, currentID } = this.state;

        return (
            <Modal show={isShowDeleteModal} onHide={this.onClickDeleteModal.bind(this)}>
                <Modal.Header>
                    <Modal.Title>Delete Category</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Are you sure deleting category {currentName} ({currentID})
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={this.onClickDeleteModal.bind(this)} type="button" className="btn btn-danger">Cancel</button>
                    <button type="button" className="btn btn-success" onClick={this.deleteCategory}>Delete</button>
                </Modal.Footer>
            </Modal>
        );
    };

    render() {
        const { categories } = this.state;

        const columns = [
            {
                name: "id",
                label: "ID",
                options: {
                    sort: true,
                },
            },
            {
                name: "name",
                label: "Name",
                options: {
                    filter: true,
                    sort: false,
                },
            },
            {
                name: "slug",
                label: "Slug",
                options: {},
            },
            {
                name: "lastUpdate",
                label: "Updated At",
                options: {
                    filter: true,
                    sort: false,
                },
            },
            {
                name: "action",
                label: "Action",
                options: {},
            },
        ];

        const options = {
            selectableRowsHeader: false,
            selectableRowsHideCheckboxes: true,
        };

        const addAction = function (category) {
            let rawUpdatedAt = new Date(category.updatedAt);
            category.lastUpdate = rawUpdatedAt.toDateString() + ' at ' + rawUpdatedAt.toLocaleTimeString();
            category.action = (
                <div>
                    <button type="button" className="btn btn-info" id={category.id} name={category.name} onClick={this.onClickUpdateModal.bind(this)}>Update</button>&nbsp;&nbsp;
                    <button type="button" className="btn btn-danger" id={category.id} name={category.name} onClick={this.onClickDeleteModal.bind(this)}>Delete</button>
                </div>
            );
            return category;
        }

        const finalCategories = categories.map(addAction.bind(this));

        return (
            <div className="col-md-12">
                {this.renderCategoryFormModal()}
                {this.renderCategoryDeletionFormModal()}
                <button type="button" className="btn btn-primary float-md-right" onClick={this.onClickCreateModal.bind(this)}>Add Category</button>
                <br />
                <br />
                <br />
                <MUIDataTable
                    title={"Category List"}
                    data={finalCategories}
                    columns={columns}
                    options={options}
                />
            </div>
        );
    }
}

export default Category;