import React, { Component } from "react";
import DataService from "../services/service";
import MUIDataTable from "mui-datatables";
import Modal from "react-bootstrap/Modal";
import swal from "sweetalert";
import {Link} from "react-router-dom";

class Article extends Component {
    constructor(props) {
        super(props);
        this.getCategories = this.getCategories.bind(this);
        this.retrieveArticles = this.retrieveArticles.bind(this);
        this.deleteArticle = this.deleteArticle.bind(this);

        this.state = {
            categories: [],
            mappedCategories: new Map(),
            articles: [],
            isShowPreviewModal: false,
            isShowDeleteModal: false,
            isShowUpdateModal: false,
            currentID: 0,
            currentTitle: '',
            currentDescription: '',
            currentCategoryID: 0,
            currentPublished: false,
        };
    }

    componentDidMount() {
        this.getCategories();
        this.retrieveArticles();
    }

    getCategories() {
        DataService.getAllCategories()
            .then(response => {
                this.setState({
                    mappedCategories: new Map(
                        response.data.map(i => [i.id, i.name])
                    ),
                    categories: response.data,
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    retrieveArticles() {
        DataService.getAllArticles()
            .then(response => {
                this.setState({
                    articles: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    deleteArticle() {
        DataService.deleteArticle(this.state.currentID)
            .then(response => {
                this.setState({
                    isShowDeleteModal: false,
                    currentID: 0,
                    currentTitle: '',
                });
                this.retrieveArticles();
                this.forceUpdate();
                swal({
                    title: "Done!",
                    text: "article is deleted in database",
                    icon: "success",
                    timer: 2000,
                    button: false
                })
                console.log(response.data);
            })
            .catch(e => {
                this.setState({
                    isShowDeleteModal: false,
                    currentID: 0,
                    currentName: '',
                });
                swal({
                    title: "Failed!",
                    text: "article is failed to delete",
                    icon: "error",
                    timer: 2000,
                    button: false
                })
                console.log(e);
            });
    }

    onClickPreviewModal(e) {
        let isShowPreviewModal = !this.state.isShowPreviewModal;

        this.setState({
            isShowPreviewModal: isShowPreviewModal,
            currentID: isShowPreviewModal ? e.target.id : 0,
            currentTitle: isShowPreviewModal ? e.target.title : '',
            currentDescription: isShowPreviewModal ? e.target.dataset.description : '',
        });
    }

    onClickDeleteModal(e) {
        let isShowDeleteModal = !this.state.isShowDeleteModal;

        this.setState({
            isShowDeleteModal: isShowDeleteModal,
            currentID: isShowDeleteModal ? e.target.id : 0,
            currentTitle: isShowDeleteModal ? e.target.title : ''
        });
    }

    renderArticlePreviewModal = () => {
        const { isShowPreviewModal, currentTitle, currentDescription } = this.state;

        const onClickModal = isShowPreviewModal ? this.onClickPreviewModal.bind(this) : null;

        return (
            <Modal show={isShowPreviewModal} onHide={this.onClickPreviewModal.bind(this)}  size="xl">
                <Modal.Header>
                    <Modal.Title>{currentTitle}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    { <div dangerouslySetInnerHTML={{ __html: currentDescription }} /> }
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={onClickModal} type="button" className="btn btn-danger">Close</button>
                </Modal.Footer>
            </Modal>
        );
    };

    renderArticleDeletionFormModal = () => {
        const { isShowDeleteModal, currentTitle, currentID } = this.state;

        return (
            <Modal show={isShowDeleteModal} onHide={this.onClickDeleteModal.bind(this)} >
                <Modal.Header>
                    <Modal.Title>Delete Article</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Are you sure deleting article <b>{currentTitle}</b> ({currentID})
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={this.onClickDeleteModal.bind(this)} type="button" className="btn btn-danger">Cancel</button>
                    <button type="button" className="btn btn-success" onClick={this.deleteArticle}>Delete</button>
                </Modal.Footer>
            </Modal>
        );
    };

    render() {
        const { mappedCategories, articles } = this.state;

        const columns = [
            {
                name: "id",
                label: "ID",
                options: {
                    sort: true,
                },
            },
            {
                name: "title",
                label: "Title",
                options: {
                    filter: true,
                    sort: true,
                },
            },
            {
                name: "category",
                label: "Category",
                options: {
                    filter: true,
                    sort: true,
                },
            },
            {
                name: "publishedText",
                label: "Status",
                options: {
                    filter: true,
                    sort: true,
                },
            },
            {
                name: "createdAtWithFormat",
                label: "Created At",
                options: {
                    filter: true,
                    sort: true,
                },
            },
            {
                name: "action",
                label: "Action",
                options: {},
            },
        ];

        const options = {
            selectableRowsHeader: false,
            selectableRowsHideCheckboxes: true,
        };

        const preProcess = function (article) {
            let rawCreatedAt = new Date(article.createdAt);
            let year = rawCreatedAt.getFullYear();
            let month = rawCreatedAt.getMonth() + 1;
            let day = rawCreatedAt.getDate();
            let path = '/' + year + '/' + month + '/' + day + '/' + article.slug + '/edit';

            article.category = mappedCategories.get(article.categoryId);
            article.publishedText = article.published ? "Published" : "Pending";
            article.createdAtWithFormat = rawCreatedAt.toDateString() + ' at ' + rawCreatedAt.toLocaleTimeString();
            article.action = (
                <div>
                    <button type="button" className="btn btn-primary btn-sm" id={article.id} title={article.title} data-description={article.description} style={{margin: "5px"}} onClick={this.onClickPreviewModal.bind(this)}>Preview</button>
                    <Link to={path} className="btn btn-info btn-sm">Update</Link>
                    <button type="button" className="btn btn-danger btn-sm" id={article.id} title={article.title} style={{margin: "5px"}} onClick={this.onClickDeleteModal.bind(this)}>Delete</button>
                </div>
            );
            return article;
        }

        const finalArticles = articles.map(preProcess.bind(this));

        return (
            <div className="col-md-12">
                {this.renderArticlePreviewModal()}
                {this.renderArticleDeletionFormModal()}
                <MUIDataTable
                    title={"Article List"}
                    data={finalArticles}
                    columns={columns}
                    options={options}
                />
            </div>
        );
    }
}

export default Article;