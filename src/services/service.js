import http from "../http-common";

class DataService {
    getAllArticles() {
        return http.get("/api/articles");
    }

    getAllPublishedArticles(limit, offset) {
        return http.get(`/api/articles/published?limit=${limit}&offset=${offset}`);
    }

    countAllPublishedArticles() {
        return http.get("/api/articles/published/count");
    }

    getArticle(id) {
        return http.get(`/api/articles/${id}`);
    }

    getArticleBySlug(year, month, date, slug) {
        return http.get(`/api/articles/${year}/${month}/${date}/${slug}`);
    }

    createArticle(data) {
        return http.post("/api/articles", data);
    }

    updateArticle(id, data) {
        return http.put(`/api/articles/${id}`, data);
    }

    deleteArticle(id) {
        return http.delete(`/api/articles/${id}`);
    }

    deleteAllArticles() {
        return http.delete(`/api/articles`);
    }

    findArticleByTitle(title) {
        return http.get(`/api/articles?title=${title}`);
    }

    getAllCategories() {
        return http.get("/api/categories");
    }

    createCategory(data) {
        return http.post("/api/categories", data);
    }

    deleteCategory(id) {
        return http.delete(`/api/categories/${id}`);
    }

    updateCategory(id, data) {
        return http.put(`/api/categories/${id}`, data);
    }

    login(data) {
        return http.post("/api/login", data);
    }
}

export default new DataService();