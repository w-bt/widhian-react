import React, { Component } from "react";
import { Switch, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import HomePage from "./components/home-page.component.js"
import ArticlesList from "./components/article.component.js"
import AddArticle from "./components/add-article.component.js"
import EditArticle from "./components/edit-article.component.js"
import SingleArticle from "./components/single-article.component.js"
import PrivateRoute from './utils/private-route';
import PublicRoute from './utils/public-route';
import Login from "./components/login.component";
import Category from "./components/category.component";
import { getUser, removeUserSession } from "./utils/common";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faInstagram, faLinkedinIn } from "@fortawesome/free-brands-svg-icons";

library.add(faInstagram, faLinkedinIn);

class App extends Component {
  constructor() {
    super();
    this.state = {
      user: ''
    };
  }

  componentDidMount() {
    this.setState({ user: getUser() });
  }

  handleLogout = () => {
    removeUserSession();
    window.location.href="/#/login";
  }

  render() {
    return (
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <Link to={"/"} className="navbar-brand">
              Widhian's Blog
            </Link>
            <div className="navbar-nav mr-auto">
              {
                this.state.user ?
                    <>
                      <li className="nav-item">
                        <Link to={"/admin/article/add"} className="nav-link">
                          Add Article
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link to={"/admin/article/list"} className="nav-link">
                          Articles
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link to={"/admin/categories"} className="nav-link">
                          Categories
                        </Link>
                      </li>
                    </>
                     : ''
              }
            </div>
            {
              this.state.user ?
                  <div className="navbar-nav pull-right">
                    <li className="nav-link">
                      Welcome {this.state.user}!
                    </li>
                    <li className="nav-item">
                      <div onClick={this.handleLogout} className="nav-link">
                        Logout
                      </div>
                    </li>
                  </div> :
                  <div className="navbar-nav pull-right">
                    <li className="nav-item">
                      <Link to="/login" className="nav-link">
                        Login
                      </Link>
                    </li>
                  </div>
            }
          </nav>

          <div className="container mt-3" style={{marginBottom: "100px"}}>
            <Switch>
              <PrivateRoute exact path="/admin/article/list" component={ArticlesList} />
              <PrivateRoute exact path="/admin/article/add" component={AddArticle} />
              <PrivateRoute exact path="/:year/:month/:date/:slug/edit" component={EditArticle} />
              <PrivateRoute exact path="/admin/categories" component={Category} />
              <PublicRoute exact path="/login" component={Login} />
              <PublicRoute exact path={["/", ""]} component={HomePage} />
              <PublicRoute path="/:year/:month/:date/:slug" component={SingleArticle} />
            </Switch>
          </div>

          <footer className="navbar navbar-expand navbar-dark bg-dark" style={{position: "fixed", left: "0", bottom: "0", width: "100%", zIndex: "999"}}>
            <div className="col-md-2 offset-md-5">
              <div className="row">
                <div className="col-sm" style={{textAlign: "center"}}>
                  <a href="https://instagram.com/widhianb" target="_blank"><FontAwesomeIcon icon={['fab', 'instagram']} inverse size="lg" /></a>
                </div>
                <div className="col-sm" style={{textAlign: "center"}}>
                  <a href="https://www.linkedin.com/in/widhianbramantya/" target="_blank"><FontAwesomeIcon icon={['fab', 'linkedin-in']} inverse size="lg" /></a>
                </div>
                <div className="col-sm" style={{textAlign: "center"}}>
                  <a href="http://dlooper.com" target="_blank"><span style={{color: "white"}}>Dlooper</span></a>
                </div>
              </div>
            </div>
          </footer>
        </div>
    );
  }
}

export default App;